﻿using System;
using System.Collections.Generic;
using System.Text;
using MessageContracts;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class UpdatePromocodesCountDto : IUpdatePromocodesCount
    {
        public Guid PartnerID { get; set; }
    }
}
