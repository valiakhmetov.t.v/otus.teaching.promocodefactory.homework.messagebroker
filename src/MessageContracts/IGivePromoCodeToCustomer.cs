﻿using System;

namespace MessageContracts
{
    public interface IGivePromoCodeToCustomer
    {
         string ServiceInfo { get; }

         Guid PartnerId { get; }

        Guid PromoCodeId { get; }

        string PromoCode { get; }

        Guid PreferenceId { get; }

        string BeginDate { get; }

        string EndDate { get; }

        Guid? PartnerManagerId { get; }
    }
}
