﻿using System;

namespace MessageContracts
{
    public interface IUpdatePromocodesCount
    {
         Guid PartnerID { get; }
    }
}
