﻿using MassTransit;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Dto;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using MessageContracts;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    public class GivePromoCodeToCustomer : IConsumer<IGivePromoCodeToCustomer>
    {
        private IPromocodeService _service;

        public GivePromoCodeToCustomer(IPromocodeService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<IGivePromoCodeToCustomer> context)
        {
            var dto = new GivePromoCodeRequest()
            {
                PartnerId = context.Message.PartnerId,
                BeginDate = context.Message.BeginDate,
                EndDate = context.Message.EndDate,
                PreferenceId = context.Message.PreferenceId,
                PromoCode = context.Message.PromoCode,
                PromoCodeId = context.Message.PromoCodeId,
                ServiceInfo = context.Message.ServiceInfo,
            };

            await _service.GiveToCustomer(dto);

            await context.RespondAsync<EmptyResponse>(new EmptyResponse());
        }
    }
}
