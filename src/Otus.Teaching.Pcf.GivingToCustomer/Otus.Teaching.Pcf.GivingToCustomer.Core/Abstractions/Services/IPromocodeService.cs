﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromocodeService
    {
        Task<IEnumerable<PromoCode>> GetAllAsync();

        Task GiveToCustomer(GivePromoCodeRequest request);
    }
}