﻿using Microsoft.Extensions.Hosting;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using MassTransit;
using MessageContracts;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.HostedServices
{
    public class UpdatePromocodesCountConsumer : IConsumer<IUpdatePromocodesCount>
    {
        private IServiceScopeFactory _serviceScopeFactory;

        public UpdatePromocodesCountConsumer(IServiceScopeFactory  service)
        {
            _serviceScopeFactory = service;
        }

        public async Task Consume(ConsumeContext<IUpdatePromocodesCount> context)
        {
            // received message  
            var id = context.Message.PartnerID;

            var employeeRepository = _serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<IRepository<Employee>>();
            var employee = await employeeRepository.GetByIdAsync(id);

            if (employee == null)
                throw new ArgumentNullException();

            employee.AppliedPromocodesCount++;

            await employeeRepository.UpdateAsync(employee);

            await context.RespondAsync<EmptyResponse>(new EmptyResponse());
        }
    }

   
}
